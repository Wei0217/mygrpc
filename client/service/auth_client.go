package service

import (
	"context"
	"time"

	"dennis.com/grpc-server/pb"
	"google.golang.org/grpc"
)

type AuthClient struct {
	authServiceClient pb.AuthServiceClient
	username          string
	password          string
}

func NewAuthClient(cc *grpc.ClientConn, username string, password string) *AuthClient {
	authServiceClient := pb.NewAuthServiceClient(cc)
	return &AuthClient{authServiceClient, username, password}
}

// user login ti get token
func (client *AuthClient) Login() (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second) // 5 second timeout
	defer cancel()

	req := &pb.LoginRequest{
		Username: client.username,
		Password: client.password,
	}

	res, err := client.authServiceClient.Login(ctx, req)
	if err != nil {
		return "", err
	}

	return res.GetAccessToken(), nil
}
