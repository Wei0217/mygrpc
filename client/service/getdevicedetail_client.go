package service

import (
	"context"
	"fmt"
	"log"

	"dennis.com/grpc-server/pb"
	"google.golang.org/grpc"
)

type GetDeviceDetailClient struct {
	getDeviceDetailClient pb.GetDeviceDetailClient
	username              string
	password              string
}

func NewGetDeviceDetailClient(cc *grpc.ClientConn, username string, password string) *GetDeviceDetailClient {
	getDeviceDetailClient := pb.NewGetDeviceDetailClient(cc)
	return &GetDeviceDetailClient{getDeviceDetailClient, username, password}
}

// user login ti get token
func (client *GetDeviceDetailClient) GetDeviceDetail() error {
	res, err := client.getDeviceDetailClient.GetCPU(context.Background(),
		&pb.GPU{
			Brand:  "NVIDIA",
			Name:   "RTX3060ti",
			MinGhz: 3.1,
			MaxGhz: 5.4,
			Memory: &pb.Memory{
				Value: 123,
				Unit:  2,
			},
		})
	if err != nil {
		log.Fatalf("GetCPU error: %v", err)
		return err
	}
	fmt.Println(res) //執行結果
	return nil
}
