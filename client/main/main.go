package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"dennis.com/grpc-client/service"
	"dennis.com/grpc-server/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	username = "admin1"
	password = "secret1"
)

var (
	// serverAddr            = "localhost:50051"
	getDeviceDetailClient pb.GetDeviceDetailClient
	authServiceClient     pb.AuthServiceClient
	tlsServerName         = "localhost"     // server證書的subject CN 名稱
	refreshDuration       = 8 * time.Second // how long the token will expired
	serverAddress         = flag.String("addr", "localhost:50051", "the server address")
	enableTLS             = flag.Bool("tls", false, "enable SSL/TLS")
)

func main() {
	// execute procedure with "addr" arg, it will be the server address, default is "localhost:50051"
	// serverAddress := flag.String("address", "localhost:50051", "the server address")
	flag.Parse()
	log.Printf("dial server %s, TLS = %t", *serverAddress, *enableTLS)

	transportOption := grpc.WithInsecure()

	// make SSL/TLS credential if user execute the program with -tls
	if *enableTLS {
		tlsCredentials, err := loadTLSCredentials() // 載入憑證
		if err != nil {
			log.Fatal("cannot load TLS credentials: ", err)
		}

		transportOption = grpc.WithTransportCredentials(tlsCredentials)
	}

	// 建立grpc連線cc1(without interceptor)
	cc1, err := grpc.Dial(*serverAddress, transportOption)
	if err != nil {
		log.Fatal("cannot dial server cc1: ", err)
	}
	defer cc1.Close()

	// new grpc client object
	authClient := service.NewAuthClient(cc1, username, password)

	// new interceptor object, and use grpc client object to invoke "Login" grpc service(get Json Web Token) without interceptor(cc1)
	// (important!!)create annother goroutine to get JWT continually
	authInterceptor, err := service.NewAuthInterceptor(authClient, authMethods(), refreshDuration)
	if err != nil {
		log.Fatal("cannot create auth interceptor: ", err)
	}

	// 建立grpc連線cc2(with interceptor)
	cc2, err := grpc.Dial(
		*serverAddress,
		transportOption,
		grpc.WithUnaryInterceptor(authInterceptor.Unary()),
		grpc.WithStreamInterceptor(authInterceptor.Stream()))
	if err != nil {
		log.Fatal("cannot dial server cc2: ", err)
	}
	defer cc2.Close()

	// new annother grpc client object, which use grpc service with inceptor(cc2)
	getDetailClient := service.NewGetDeviceDetailClient(cc2, username, password)
	getDetailClient.GetDeviceDetail()

	for {
		time.Sleep(10 * time.Second)
	}

	// getDeviceDetailClient = pb.NewGetDeviceDetailClient(cc2) // 用 proto 提供的 NewStudentServerClient, 來建立 client, 可用來調用grpc
	// authServiceClient = pb.NewAuthServiceClient(cc1)         // 用 proto 提供的 NewAuthServiceClient, 來建立 client, 可用來調用grpc

	// 本地function調用grpc(old method, invoke grpc directly, not package the grpc to a struct)
	// GetCPU()
	// UserLogin()
}

// 利用client來使用grpc
func GetCPU() {
	res, err := getDeviceDetailClient.GetCPU(context.Background(),
		&pb.GPU{
			Brand:  "NVIDIA",
			Name:   "RTX3060ti",
			MinGhz: 3.1,
			MaxGhz: 5.4,
			Memory: &pb.Memory{
				Value: 123,
				Unit:  2,
			},
		})
	if err != nil {
		log.Fatalf("GetCPU error: %v", err)
	}
	fmt.Println(res) //執行結果
}

func UserLogin() {
	res, err := authServiceClient.Login(context.Background(),
		&pb.LoginRequest{
			Username: "admin1",
			Password: "secret1",
		})
	if err != nil {
		log.Fatalf("UserLogin error: %v", err)
	}
	fmt.Println(res) //執行結果
}

// 加載憑證
func loadTLSCredentials() (credentials.TransportCredentials, error) {
	// Load certificate of the CA who signed server's certificate
	pemServerCA, err := ioutil.ReadFile("cert/ca-cert.pem")
	if err != nil {
		return nil, err
	}

	// 證書池
	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemServerCA) {
		return nil, fmt.Errorf("failed to add server CA's certificate")
	}

	// Load client's certificate and private key
	clientCert, err := tls.LoadX509KeyPair("cert/client-cert.pem", "cert/client-key.pem")
	if err != nil {
		return nil, err
	}

	// Create the credentials and return it
	config := &tls.Config{
		Certificates: []tls.Certificate{clientCert},
		RootCAs:      certPool,
		// ServerName: tlsServerName, // NOTE: this is required!(in server-side-only verify)
	}

	return credentials.NewTLS(config), nil
}

func authMethods() map[string]bool {
	const myServicePackagePath = "/pb."

	return map[string]bool{
		myServicePackagePath + "AuthService/Login":      true, // 此method需要驗證(attach token)
		myServicePackagePath + "GetDeviceDetail/GetCPU": true, // 此method需要驗證(attach token)
	}
}
