package service

import (
	"context"

	"dennis.com/grpc-server/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AuthServer struct {
	userStore  UserStore
	jwtManager *JWTManager
	pb.UnimplementedAuthServiceServer
}

func NewAuthServer(userStore UserStore, jwtManager *JWTManager) *AuthServer {
	authServer := &AuthServer{}
	authServer.userStore = userStore
	authServer.jwtManager = jwtManager
	return authServer
}

// user login
func (server *AuthServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	user, err := server.userStore.Find(req.GetUsername())
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot find user: %v", err) // 理論上，Find不會return err，所以如果跑到這，完蛋了
	}

	if user == nil || !user.IsCorrectPassword(req.GetPassword()) { // username不存在，或user密碼錯誤
		return nil, status.Errorf(codes.NotFound, "incorrect username/password")
	}

	token, err := server.jwtManager.Generate(user) // user密碼正確，創造token
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot generate access token")
	}

	res := &pb.LoginResponse{AccessToken: token}
	return res, nil
}
