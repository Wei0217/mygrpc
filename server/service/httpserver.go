package service

import (
	"context"
	"net/http"

	"dennis.com/grpc-server/pb"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

// var (
// // command-line options:
// // gRPC server endpoint
// // grpcServerEndpoint = flag.String("gse", "localhost:9090", "gRPC server endpoint")
// )

func run(grpcServerEndpoint, httpServerEndpoint string, cred *credentials.TransportCredentials) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Register gRPC server endpoint
	// Note: Make sure the gRPC server is running properly and accessible
	mux := runtime.NewServeMux()
	var opts []grpc.DialOption
	if *cred != nil {
		opts = []grpc.DialOption{grpc.WithTransportCredentials(*cred)}
	} else {
		opts = []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
		// opts = []grpc.DialOption{grpc.WithInsecure()}
	}

	if err := pb.RegisterGetDeviceDetailHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		return err
	}
	if err := pb.RegisterAuthServiceHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts); err != nil {
		return err
	}

	// Start HTTP server (and proxy calls to gRPC server endpoint)
	return http.ListenAndServe(httpServerEndpoint, mux)
}

func HttpMain(grpcAddr string, httpAddr string, cred *credentials.TransportCredentials) error {
	// flag.Parse()
	// defer glog.Flush()
	if err := run(grpcAddr, httpAddr, cred); err != nil {
		// glog.Fatal(err)
		return err
	}
	return nil
}
