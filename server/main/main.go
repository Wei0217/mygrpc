package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"dennis.com/grpc-server/pb"
	"dennis.com/grpc-server/service"

	// _ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway"
	// _ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2"
	"google.golang.org/grpc"
	// _ "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
	// _ "google.golang.org/protobuf/cmd/protoc-gen-go"
)

const (
	secretKey     = "mysecret"
	tokenDuration = 5 * time.Minute
)

var (
	grpcPort         = flag.Int("gport", 50051, "The Grpc server port")
	httpPort         = flag.Int("hport", 8081, "The Http server port")
	enableTLS        = flag.Bool("tls", false, "enable SSL/TLS")
	shutdownObserver = make(chan os.Signal, 1)
	tlsCredentials   credentials.TransportCredentials
	HttpServerCred   credentials.TransportCredentials
)

// server is used to implement GetDeviecDetailServer.
type server struct {
	pb.UnimplementedGetDeviceDetailServer
}

// SayHello implements GetDeviecDetailServer
func (s *server) GetCPU(ctx context.Context, in *pb.GPU) (*pb.CPU, error) {
	log.Printf("Received: GPU: %v\n", in)
	// log.Printf()
	return &pb.CPU{Brand: "intel", Name: "12th i5-12400", NumberCores: 6, NumberThreads: 12, MinGhz: 2.5, MaxGhz: 5.9}, nil
}

func main() {
	flag.Parse()
	log.Printf("start Grpc server on port %d\nstart Http server on port %d\nTLS = %t", *grpcPort, *httpPort, *enableTLS)

	// in-memoru DB initialize
	userStore := service.NewInMemoryUserStore()

	// auto create some(2) users to UserStore
	if err := seedUsers(userStore); err != nil {
		log.Fatal("cannot seed users: ", err)
	}

	// JWT manager initialize
	jwtManager := service.NewJWTManager(secretKey, tokenDuration)

	// AuthServer grpc service object
	authServer_service := service.NewAuthServer(userStore, jwtManager)
	// GetDeviceDetail grpc service object
	getDeviceDetail_service := &server{}

	// Interceptor object, accessibleRoles() define what user role can access the grps method
	interceptor := service.NewAuthInterceptor(jwtManager, accessibleRoles())

	// make grpcserver's option, add interceptors
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(InterceptChain(unaryInterceptor, interceptor.Unary())),
		grpc.StreamInterceptor(interceptor.Stream()),
	}

	// make grpcserver's option, add SSL/TLS credential if user execute the program with -tls
	if *enableTLS {
		var err error
		// 載入憑證
		tlsCredentials, err = loadServerTLSCredentials()
		if err != nil {
			log.Fatal("cannot load Server TLS credentials: ", err)
		}

		serverOptions = append(serverOptions, grpc.Creds(tlsCredentials))

		HttpServerCred, err = loadClientTLSCredentials()
		if err != nil {
			log.Fatal("cannot load Client TLS credentials: ", err)
		}
	}

	// 使用 gRPC 的 NewServer meethod 來建立 gRPC Server(with cred, some interceptors)
	grpcServer := grpc.NewServer(serverOptions...)
	// grpcServer := grpc.NewServer(
	// 	grpc.Creds(tlsCredentials), // with credential
	// 	// grpc.UnaryInterceptor(unaryInterceptor),      // test interceptor(midware)
	// 	// grpc.UnaryInterceptor(interceptor.Unary()),   // grpc method permission(midware)
	// 	grpc.UnaryInterceptor(InterceptChain(unaryInterceptor, interceptor.Unary())), // 為grpc server放兩個unaryInterceptor
	// 	grpc.StreamInterceptor(interceptor.Stream()),                                 // grpc method permission(midware)
	// )

	pb.RegisterGetDeviceDetailServer(grpcServer, getDeviceDetail_service) // register "GetDeviceDetail" grpc service
	pb.RegisterAuthServiceServer(grpcServer, authServer_service)          // register "AuthService" grpc service

	// 在 gRPC 伺服器上註冊反射服務(Client可使用Evans CLI進行測試)
	reflection.Register(grpcServer)

	// // 玩玩的，不能用
	// router := gin.Default()
	// router.POST("/pb.GetDeviceDetail/GetCPU", func(ctx *gin.Context) {
	// 	fmt.Println(ctx.Request)
	// 	grpcServer.ServeHTTP(ctx.Writer, ctx.Request)
	// })
	// router.RunTLS(fmt.Sprintf(":%v", *port), "cert/server-cert.pem", "cert/server-key.pem")

	// 用goroutine啟動監聽
	go func(gs *grpc.Server) {
		// 設定要監聽的 port
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *grpcPort))
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}

		log.Println("等待Grpc client連接")
		err = gs.Serve(lis) // 堵塞
		if err != nil {
			log.Println("Grpc client", err)
			shutdownObserver <- syscall.SIGINT // 有錯誤就塞入信號，讓main go優雅關閉
		}
	}(grpcServer)

	go func() {
		log.Println("等待Http client連接")

		err := service.HttpMain(fmt.Sprintf("localhost:%d", *grpcPort), fmt.Sprintf("localhost:%d", *httpPort), &HttpServerCred)
		if err != nil {
			log.Println("Http client", err)
			shutdownObserver <- syscall.SIGINT // 有錯誤就塞入信號，讓main go優雅關閉
		}
	}()

	// 設定想要收到哪種信號
	signal.Notify(shutdownObserver, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)

	//main go阻塞直到有信號傳入
	s := <-shutdownObserver
	log.Println("Receive signal:", s)

	// 優雅停止GRPC服務
	grpcServer.GracefulStop()
}

// 加載Server憑證
func loadServerTLSCredentials() (credentials.TransportCredentials, error) {
	// Load certificate of the CA who signed client's certificate
	pemClientCA, err := ioutil.ReadFile("cert/ca-cert.pem")
	if err != nil {
		return nil, err
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemClientCA) {
		return nil, fmt.Errorf("failed to add client CA's certificate")
	}

	// Load server's certificate and private key
	serverCert, err := tls.LoadX509KeyPair("cert/server-cert.pem", "cert/server-key.pem")
	if err != nil {
		return nil, err
	}

	// Create the credentials and return it
	config := &tls.Config{
		Certificates: []tls.Certificate{serverCert},
		// ClientAuth:   tls.NoClientCert, // server-side單向認證
		ClientAuth: tls.RequireAndVerifyClientCert, // server-client雙向認證
		ClientCAs:  certPool,
	}

	return credentials.NewTLS(config), nil
}

// 加載Client憑證, for rest api, 需要直接拿client的cred, 類似代理用client cred去invoke grpc
func loadClientTLSCredentials() (credentials.TransportCredentials, error) {
	// Load certificate of the CA who signed server's certificate
	pemServerCA, err := ioutil.ReadFile("cert/ca-cert.pem")
	if err != nil {
		return nil, err
	}

	// 證書池
	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemServerCA) {
		return nil, fmt.Errorf("failed to add server CA's certificate")
	}

	// Load client's certificate and private key
	clientCert, err := tls.LoadX509KeyPair("cert/client-cert.pem", "cert/client-key.pem")
	if err != nil {
		return nil, err
	}

	// Create the credentials and return it
	config := &tls.Config{
		Certificates: []tls.Certificate{clientCert},
		RootCAs:      certPool,
		// ServerName: tlsServerName, // NOTE: this is required!(in server-side-only verify)
	}

	return credentials.NewTLS(config), nil
}

// 類似midware，在進入rpc業務處理前，先跑這裡的代碼(just test)
func unaryInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (interface{}, error) {
	log.Println("unaryInterceptor test: ", req)
	log.Println("--> unary interceptor test: ", info.FullMethod)
	return handler(ctx, req)
}

// create user
func createUser(userStore service.UserStore, username, password, role string) error {
	user, err := service.NewUser(username, password, role)
	if err != nil {
		return err
	}
	return userStore.Save(user)
}

// make 2 users for test
func seedUsers(userStore service.UserStore) error {
	err := createUser(userStore, "admin1", "secret1", "admin")
	if err != nil {
		return err
	}
	return createUser(userStore, "user1", "secret2", "user")
}

// define grpc method's permission role(which role can access the grpc method)
func accessibleRoles() map[string][]string {
	const myServicePackagePath = "/pb."

	return map[string][]string{
		// myServicePackagePath + "AuthService/Login":      {"admin", "user"}, // admin, user可以使用
		myServicePackagePath + "GetDeviceDetail/GetCPU": {"admin"},
	}
}

// interceptor chain,cause can only set one unary inceptor to grpc server, so use this func to make a interceptor chain
func InterceptChain(intercepts ...grpc.UnaryServerInterceptor) grpc.UnaryServerInterceptor {
	//获取拦截器的长度
	l := len(intercepts)
	//如下我们返回一个拦截器
	return func(ctx context.Context, req interface{},
		info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		//在这个拦截器中，我们做一些操作
		//构造一个链
		chain := func(currentInter grpc.UnaryServerInterceptor, currentHandler grpc.UnaryHandler) grpc.UnaryHandler {
			return func(currentCtx context.Context, currentReq interface{}) (interface{}, error) {
				return currentInter(
					currentCtx,
					currentReq,
					info,
					currentHandler)
			}
		}
		//声明一个handler
		chainHandler := handler
		for i := l - 1; i >= 0; i-- {
			//递归一层一层调用
			chainHandler = chain(intercepts[i], chainHandler)
		}
		//返回结果
		return chainHandler(ctx, req)
	}
}
