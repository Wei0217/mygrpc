// 所有proto檔案皆放在同一資料夾(/pb)，cd到那資料夾(/pb)，輸入以下指令
// protoc -I D:/goPath/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis -I . --go_out . --go_opt paths=source_relative --go-grpc_out . --go-grpc_opt paths=source_relative --grpc-gateway_out . --grpc-gateway_opt logtostderr=true --grpc-gateway_opt paths=source_relative *.proto

// paths=source_relative 生成檔案在proto檔案的位址
// https://stackoverflow.com/questions/70731053/protoc-go-opt-paths-source-relative-vs-go-grpc-opt-paths-source-relative
// --proto_path=mypb(別名-I) 在下指令位址的哪個相對路徑裡(或直接打絕對路徑)，找該proto檔案裡的import依賴，沒有該參數的話，默認當前路徑，只要有該參數，就不會使用默認值，可寫多個
// --go_out . 表示生成的檔案在當前工作目錄，其他相似的參數，同理

// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v4.23.4
// source: devicedetail.proto

// option java_multiple_files = true;
// option java_package = "io.grpc.examples.helloworld";
// option java_outer_classname = "HelloWorldProto";

package pb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	GetDeviceDetail_GetCPU_FullMethodName = "/pb.GetDeviceDetail/GetCPU"
)

// GetDeviceDetailClient is the client API for GetDeviceDetail service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type GetDeviceDetailClient interface {
	// Sends a GPU, Get CPU
	GetCPU(ctx context.Context, in *GPU, opts ...grpc.CallOption) (*CPU, error)
}

type getDeviceDetailClient struct {
	cc grpc.ClientConnInterface
}

func NewGetDeviceDetailClient(cc grpc.ClientConnInterface) GetDeviceDetailClient {
	return &getDeviceDetailClient{cc}
}

func (c *getDeviceDetailClient) GetCPU(ctx context.Context, in *GPU, opts ...grpc.CallOption) (*CPU, error) {
	out := new(CPU)
	err := c.cc.Invoke(ctx, GetDeviceDetail_GetCPU_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// GetDeviceDetailServer is the server API for GetDeviceDetail service.
// All implementations must embed UnimplementedGetDeviceDetailServer
// for forward compatibility
type GetDeviceDetailServer interface {
	// Sends a GPU, Get CPU
	GetCPU(context.Context, *GPU) (*CPU, error)
	mustEmbedUnimplementedGetDeviceDetailServer()
}

// UnimplementedGetDeviceDetailServer must be embedded to have forward compatible implementations.
type UnimplementedGetDeviceDetailServer struct {
}

func (UnimplementedGetDeviceDetailServer) GetCPU(context.Context, *GPU) (*CPU, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCPU not implemented")
}
func (UnimplementedGetDeviceDetailServer) mustEmbedUnimplementedGetDeviceDetailServer() {}

// UnsafeGetDeviceDetailServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to GetDeviceDetailServer will
// result in compilation errors.
type UnsafeGetDeviceDetailServer interface {
	mustEmbedUnimplementedGetDeviceDetailServer()
}

func RegisterGetDeviceDetailServer(s grpc.ServiceRegistrar, srv GetDeviceDetailServer) {
	s.RegisterService(&GetDeviceDetail_ServiceDesc, srv)
}

func _GetDeviceDetail_GetCPU_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GPU)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GetDeviceDetailServer).GetCPU(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: GetDeviceDetail_GetCPU_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GetDeviceDetailServer).GetCPU(ctx, req.(*GPU))
	}
	return interceptor(ctx, in, info, handler)
}

// GetDeviceDetail_ServiceDesc is the grpc.ServiceDesc for GetDeviceDetail service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var GetDeviceDetail_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "pb.GetDeviceDetail",
	HandlerType: (*GetDeviceDetailServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCPU",
			Handler:    _GetDeviceDetail_GetCPU_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "devicedetail.proto",
}
